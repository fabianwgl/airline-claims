const bcrypt = require('bcryptjs');

const utils = require('../../../utils');
const User = require('../../db/models').user;

module.exports.createUser = (req, res) => {
  const hashedPassword = bcrypt.hashSync(req.body.password, 8);
  User.create({
    name: req.body.name,
    lastname: req.body.lastname,
    email: req.body.email,
    password: hashedPassword,
    role: 'user',
  })
    .then((user) => {
      const token = utils.createToken(user.id);
      res.status(200).send({ auth: true, token });
    })
    .catch(error => res.status(400).send(error.message));
};

module.exports.getMe = (req, res) => {
  User.findById(req.user.id, {
    attributes: ['id', 'name', 'lastname'],
  })
    .then((user) => {
      const token = utils.createToken(user.id);
      res.status(200).send({ user, token });
    })
    .catch(error => res.status(400).send(error.message));
};

module.exports.login = (req, res) => {
  if (req.body.email && req.body.password) {
    User.findOne({ where: { email: req.body.email } })
      .then((user) => {
        if (user) {
          if (bcrypt.compareSync(req.body.password, user.password)) {
            const token = utils.createToken(user.id);
            res.status(200).send({ user, token });
          }
        } else {
          res.status(400).send('user not found');
        }
      })
      .catch(err => res.status(400).send(err.message));
  }
};
