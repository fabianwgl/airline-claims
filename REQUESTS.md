
# Users

Api concerning users

## Login

Used to collect a Token for a registered User.

**URL** : `/user/login`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
    "email": "[valid email address]",
    "password": "[password in plain text]"
}
```

## Create

Create a user a get a token

**URL** : `/user/create`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
    "name": "[lastname in plain text]",
    "lastname": "[lastname in plain text]",
    "email": "[valid email address]",
    "password": "[password in plain text]"
}
```


# Flights

Api concerning flights

## All

Gets all flights in db

**URL** : `/flight/all`

**Method** : `GET`

**Auth required** : Header token



# Ticket

Api concerning tickets and messages

## Create

Create a ticket 

**URL** : `/ticket/create`

**Method** : `POST`

**Auth required** : Header token

**Data constraints**
```json
{
    "flightCode": "[existing flight code]",
    "title": "[claim title]",
    "description": "[claim description]",
}
```
## All

Fetch all tickets

**URL** : `/ticket/`

**Method** : `GET`

**Auth required** : Admin header token


## By status

Fetch tickets by status

**URL** : `/ticket/:status`

**Method** : `GET`

**Auth required** : Admin header token

## Assign

Assign a ticket to logged admin

**URL** : `/ticket/assign`

**Method** : `PUT`

**Auth required** : Admin header token

**Data constraints**
```json
{
    "ticketId": "[existing ticket id]",
}
```
## Update status

Updates a ticket status

**URL** : `/ticket/status`

**Method** : `PUT`

**Auth required** : Admin header token

**Data constraints**
```json
{
    "ticketId": "[existing ticket id]",
    "status": "[pending, wip or resolved]"
}
```

## Add message

add a message into a ticket 

**URL** : `/ticket/:id`

**Method** : `POST`

**Auth required** : Header token

**Data constraints**
```json
{
    "ticketId": "[existing ticket id]",
    "message": "[message in plain text]"
}
```
