module.exports = (sequelize, DataTypes) => {
  const ticket = sequelize.define('ticket', {
    reclaimer_id: DataTypes.INTEGER,
    solver_id: DataTypes.INTEGER,
    flight_id: DataTypes.INTEGER,
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    status: {
      type: DataTypes.ENUM,
      values: ['pending', 'wip', 'resolved'],
    },
  }, {
    freezeTableName: true,
  });
  ticket.associate = (models) => {
    //  ticket.belongsTo(models.user, { foreignKey: 'reclaimer_id', targetKey: 'id' });
    //  ticket.belongsTo(models.user, { foreignKey: 'solver', targetKey: 'id' });
  };
  return ticket;
};
