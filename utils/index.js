const jwt = require('jsonwebtoken');

const config = require('../src/config');

module.exports.createToken = (id) => {
  const token = jwt.sign({ id }, config.secret, {
    expiresIn: 86400,
  });
  return token;
};
