const datefns = require('date-fns');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('flight', [{
      code: 'AAAA5001',
      date: datefns.format(new Date(), 'YYYY-MM-DD HH:mm'),
      createdAt: datefns.format(new Date(), 'YYYY-MM-DD HH:mm'),
      updatedAt: datefns.format(new Date(), 'YYYY-MM-DD HH:mm'),
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('flight', null, {});
  }
};
