module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    name: DataTypes.STRING,
    lastname: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    role: {
      type: DataTypes.ENUM,
      values: ['user', 'admin'],
    },
  }, {
    freezeTableName: true,
  });
  User.associate = (models) => {
    User.hasMany(models.ticket, {
      foreignKey: 'reclaimer_id',
      sourceKey: 'id',
    });
    User.hasMany(models.ticket, {
      foreignKey: 'solver_id',
      sourceKey: 'id',
    });
  };
  return User;
};
