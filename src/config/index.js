module.exports = {
  PORT: process.env.PORT || 5000,
  secret: process.env.SECRET || 'nubesnubes',
};
