module.exports = (sequelize, DataTypes) => {
  const flight = sequelize.define('flight', {
    code: DataTypes.STRING,
    date: DataTypes.DATE,
  }, {
    freezeTableName: true,
  });
  flight.associate = (models) => {
    /*
    flight.hasMany(models.ticket, {
      foreignKey: 'flight_id',
      sourceKey: 'id',
    });
    */
  };
  return flight;
};
