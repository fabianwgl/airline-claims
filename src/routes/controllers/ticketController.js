const waterfall = require('async/waterfall');

const Ticket = require('../../db/models').ticket;
const Flight = require('../../db/models').flight;

module.exports.createTicket = (req, res) => {
  waterfall([
    (cb) => {
      Flight.find({ code: req.body.flightCode })
        .then(flight => cb(null, flight))
        .catch(err => cb(err));
    },
    (flight, cb) => {
      Ticket.create({
        reclaimer_id: req.user.id,
        flight_id: flight.id,
        title: req.body.title,
        description: req.body.description,
        status: 'pending',
      })
        .then(ticket => cb(null, ticket))
        .catch(ticketError => cb(ticketError));
    },
  ], (error, data) => {
    if (error) {
      res.status(400).send(error.message);
    } else {
      res.status(201).send(data);
    }
  });
};

module.exports.getAllTickets = (req, res) => {
  const query = { where: {} };
  if (req.body.status || req.params.status || req.query.status) {
    query.where.status = req.body.status || req.query.status || req.params.status;
  }
  Ticket.findAll(query)
    .then(allTickets => res.status(200).send(allTickets))
    .catch(err => res.status(401).send(err));
};

const updateTicket = (ticketId, query) => {
  const ticketPromise = new Promise((resolve, reject) => {
    if (ticketId) {
      Ticket.findById(ticketId)
        .then((ticket) => {
          ticket.update(query)
            .then(tkt => resolve(tkt))
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    } else {
      reject(new Error('must specify ticket id'));
    }
  });
  return ticketPromise;
};

module.exports.assignToTicket = (req, res) => {
  const query = {
    solver_id: req.user.id,
    status: 'wip',
  };
  updateTicket(req.body.ticketId, query)
    .then(tkt => res.status(200).send(tkt))
    .catch(err => res.status(400).send(err));
};

module.exports.updateTicketStatus = (req, res) => {
  const query = {
    status: req.body.status,
  };
  updateTicket(req.body.ticketId, query)
    .then(tkt => res.status(200).send(tkt))
    .catch(err => res.status(400).send(err));
};
