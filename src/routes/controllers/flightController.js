const Flight = require('../../db/models').flight;

module.exports.getAllFlights = (req, res) => {
  Flight.findAll({
    attributes: ['code', 'date'],
  })
    .then(all => res.status(201).send(all))
    .catch(err => res.status(400).send(err.message));
};
