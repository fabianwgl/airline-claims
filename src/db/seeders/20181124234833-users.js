const datefns = require('date-fns');
const bcrypt = require('bcryptjs');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const hashedPassword = bcrypt.hashSync('ohnojanedoe', 8);
    return queryInterface.bulkInsert('user', [{
      name: 'Jaime',
      lastname: 'Doe',
      email: 'jaime@doe.org',
      password: hashedPassword,
      role: 'admin',
      createdAt: datefns.format(new Date(), 'YYYY-MM-DD HH:mm'),
      updatedAt: datefns.format(new Date(), 'YYYY-MM-DD HH:mm'),
    }], {});
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user', null, {});
  },
};
