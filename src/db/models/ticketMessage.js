module.exports = (sequelize, DataTypes) => {
  const ticketMessage = sequelize.define('ticketMessage', {
    ticket_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    message: DataTypes.STRING,
  }, {
    freezeTableName: true,
  });
  ticketMessage.associate = (models) => {

  };
  return ticketMessage;
};
