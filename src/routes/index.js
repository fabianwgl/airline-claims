const express = require('express');

const middlewares = require('./middlewares');
const userController = require('./controllers/userController');
const flightController = require('./controllers/flightController');
const ticketController = require('./controllers/ticketController');
const ticketMessageController = require('./controllers/ticketMessageController');

const router = express.Router();

router.get('/', (req, res) => {
  res.send('hello');
});

router.post('/user/create', userController.createUser);
router.post('/user/login', userController.login);
router.get('/user/me', middlewares.validateToken, userController.getMe);


router.get('/flight/all', middlewares.validateToken, flightController.getAllFlights);

router.post('/ticket/create', middlewares.validateToken, ticketController.createTicket);
router.get('/ticket/', middlewares.validateToken, middlewares.isAdmin, ticketController.getAllTickets);
router.get('/ticket/:status', middlewares.validateToken, middlewares.isAdmin, ticketController.getAllTickets);
router.put('/ticket/assign', middlewares.validateToken, middlewares.isAdmin, ticketController.assignToTicket);
router.put('/ticket/status', middlewares.validateToken, middlewares.isAdmin, ticketController.updateTicketStatus);
router.post('/ticket/:id/addmessage', middlewares.validateToken, ticketMessageController.addMessage);

module.exports = router;
