const express = require('express');
const dotenv = require('dotenv');
const logger = require('morgan');
const bodyParser = require('body-parser');

const router = require('./src/routes');
const config = require('./src/config');

dotenv.config();

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(router);

app.listen(config.PORT, () => {
  console.log(`Example app listening on port ${config.PORT}`);
});
