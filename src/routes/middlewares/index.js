const jwt = require('jsonwebtoken');

const User = require('../../db/models').user;
const config = require('../../config');

module.exports.validateToken = (req, res, next) => {
  const token = req.headers['x-access-token'] || req.headers.token;
  if (token) {
    req.headers.token = token;
    jwt.verify(token, config.secret, (err, decoded) => {
      if (decoded) {
        req.user = {
          id: decoded.id,
        };
        next();
      } else {
        res.status(401).send({ auth: false, message: 'Could not find user' });
      }
    });
  } else {
    res.status(401).send({ auth: false, message: 'No token provided.' });
  }
};

module.exports.isAdmin = (req, res, next) => {
  User.findById(req.user.id)
    .then((user) => {
      if (user.role === 'admin') {
        next();
      } else {
        res.status(401).send({ auth: false, message: 'Only admin allowed' });
      }
    });
};
