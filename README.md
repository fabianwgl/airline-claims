
# Airline claims

Software for making and handling airline claim tickets

## Getting Started



### Prerequisites

-	Postgresql
-	Sequelize-cli
-	NodeJS

### Installing

If is the first time running this project, run migrations and seed

```
npm install
sequelize db:migrate
sequelise db:seed:all
```
Start project
```
npm run start
```

Create a user and get a token with HTTPie
```
echo '{
	"name": "don",
	"lastname": "caballero",
	"email": "don@caballero.us",
	"password": "1234"
}' |  \
  http POST http://localhost:5000/user/create \
  content-type:application/json
 ```

## Running the tests
### coding style tests

will run linter with airbnb configuration

```
npm run pretest
```

## Built With

* [Sequelize](http://docs.sequelizejs.com/) - Promise-based ORM
* [NPM](https://www.npmjs.com/) - Dependency Management

## Authors

* **Fabian General**  - [GitHub](https://github.com/fabianwgl)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Not a single drop of coffee was drunk in the development of this project
