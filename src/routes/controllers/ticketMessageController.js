const waterfall = require('async/waterfall');

const Ticket = require('../../db/models').ticket;
const TicketMessage = require('../../db/models').ticketMessage;

module.exports.addMessage = (req, res) => {
  if (req.body.ticketId) {
    waterfall([
      (cb) => {
        Ticket.findByPk(req.body.ticketId)
          .then(ticket => cb(null, ticket))
          .catch(err => cb(err));
      },
      (ticket, cb) => {
        TicketMessage.create({
          ticket_id: ticket.id,
          user_id: req.user.id,
          message: req.body.message,
        })
          .then(ticketMessage => cb(null, ticket, ticketMessage))
          .catch(err => cb(err));
      },
      (ticket, ticketMessage, cb) => {
        TicketMessage.findAll({ ticket_id: ticket.id })
          .then((tickets) => {
            cb(null, { ticket, tickets });
          })
          .catch(err => cb(err));
      },
    ], (err, results) => {
      if (err) {
        res.status(400).send(err.message);
      } else {
        res.status(201).send(results);
      }
    });
  } else {
    res.status(400).send('must provide ticket id');
  }
};
